API REST para gestionar productos y sus usuarios asociados.

Modelo:
    - Producto
        Id.: int
        Marca: String
        Descripcion: String
        Precio: double
        Usuarios[]
    - Usuario
        Id.: int
        Nombre: string

API REST:
/productos/ -> Colección de los productos. GET, POST.
/productos/{id} -> Un producto en particular. GET, PUT,DELETE,PATCH.
/productos/{id}/usuarios/ -> Colección de los usuarios del producto {id}. GET, POST.
/productos/{id_prod}/usuarios/{id_usuario} -> Usuario particular del producto {id_prod}. GET, DELETE, PUT, PATCH.
